const prod1 = { id: 1, name: 'Esponja', price: 50 }
const prodFieldsNull = { name: '', price: '' }
const prod3 = { id: 2, name: 'Leite', price: 2.50 }
const prod4 = { id: 3, name: 'Lâmina', price: 5 }
const prod5 = { id: 4, name: 'Leite', price: 6 }
const prod6 = { id: 5, name: 'Macarrão', price: 50 }
const prod7 = { id: 6, name: 'Leite', price: 15 }
const prod8 = { id: 7, name: 'Esponja', price: 50 }

export { 
    prod1,
    prodFieldsNull,
    prod3,
    prod4,
    prod5,
    prod6,
    prod7,
    prod8,
}