import ProductDomainService from '../../../../../src/app/functions/product'
import { 
    prod1,
    prodFieldsNull,
    prod3,
    prod4,
    prod5,
    prod6,
    prod7,
    prod8,
} from './mock'

describe('TEST DOMAIN SERVICE PRODUCT', () => {
    const domainService = ProductDomainService()
    describe('CRUD PRODUCT', () => {
        describe('CREATE PRODUCT', () => {
            it('Fields name and price is not null', () => {
                const result = 'Fields name and price is not null'
                expect(domainService.add(prodFieldsNull)).toBe(result)
            })
            it('Product creadted with sucess', () => {
                expect(domainService.add(prod1)).toHaveProperty('Message')
                expect(domainService.add(prod3)).toHaveProperty('Product')
            })
        })
        describe('RETURNING ALL PRODUCTS', () => {
            it('All products', () => {
                const result = [ prod1, prod3 ]
                expect(domainService.getAll()).toEqual(result)
            })
        })
        describe('RETURNING PRODUCT BY ID', () => {
            it('One product', () => {
                const id = 1
                expect(domainService.getOne(id)).toEqual(prod1)
            })
            it('Product not found', () => {
                const id = 5
                const result = { 'Message': 'Product not found' }
                expect(domainService.getOne(id)).toHaveProperty('Message')
                expect(domainService.getOne(id)).toEqual(result)
            })
        })
        describe('UPDATE PRODUCT BY ID', () => {
            it('Product updated with success', () => {
                const id = 1
                expect(domainService.update(id, prod3)).toHaveProperty('Message', 'Product updated with success')
            })
            it('Product not found', () => {
                const id = 5
                const result = { 'Message': 'Product not found' }
                expect(domainService.update(id, prod3)).toHaveProperty('Message')
                expect(domainService.update(id, prod3)).toEqual(result)
            })
        })
        describe('DELETE PRODUCT BY ID', () => {
            it('Product deleted with success', () => {
                const id = 1
                const result = { 'Message': 'Product deleted with sucess' }
                expect(domainService.remove(id)).toEqual(result)
            })
            it('Product not found', () => {
                const id = 5
                const result = { 'Message': 'Product not found' }
                expect(domainService.update(id, prod3)).toHaveProperty('Message')
                expect(domainService.remove(id)).toEqual(result)
            })
        })
    }) 
})