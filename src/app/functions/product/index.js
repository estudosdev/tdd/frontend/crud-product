const Product = () => {
    const products = []

    const add = ({ name, price }) => {
        if (name === '' || price === '') return 'Fields name and price is not null'
        const id = products.length + 1        
        products.push({ id, name, price })
        return { 'Message': 'Product cadastred with success', 'Product': { id, name, price } }
    }    

    const getAll = ()=> {        
        return products
    }

    const getOne = (id) => {
        const productFilted = products.filter(prod => prod.id === id)
        
        if (productFilted.length < 1) return { 'Message': 'Product not found' }

        const product = productFilted[0]
        return product
    }

    const update = (id, { name, price }) => {
        let product = getOne(id)
        
        if (product.Message) return product

        if (name === '' || price === '') return 'Fields name and price is not null'
        
        product = { ...product, name, price }
        
        return { 'Message': 'Product updated with success', 'Product': product }
    }

    const remove = (id) => {        
        const productDeleted = products.filter((prod, index) => {
            return prod.id === id ? products.splice(index, 1) : null
        })

        if (productDeleted.length <= 0) return { 'Message': 'Product not found' }

        return { 'Message': 'Product deleted with sucess' }
    }

    return {
        add,
        getAll,
        getOne,
        update,
        remove
    }
}

export default Product