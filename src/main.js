import ProductDomainService from './app/functions/product'

function main(){   
    const productDomainService = ProductDomainService()
    const newProduct = {
        id: 1,
        name: 'Cebela',
        value: 50
    }
    
    productDomainService.add(newProduct)
    const products = productDomainService.getAll()
    console.log(products)
    const productFilted = productDomainService.getOne(newProduct.id)
    console.log(productFilted)
}

main()